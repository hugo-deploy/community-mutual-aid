+++
fragment = "content"
weight = 100

header = "Timebank"
title = "Labor swapping network"
date = "2020-04-03"
categories = ["Mutual Aid"]
+++

Timebanking is a time-based currency. Give one hour of service to another, and receive one time credit. You can use the credits in turn to receive services — or you can donate them to others.  

<!--more-->

## Learn more about Time Banking
[Learn More](https://timebanks.org/timebankingabout/)  