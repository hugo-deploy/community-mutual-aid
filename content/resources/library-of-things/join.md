+++
fragment = "contact"
#disabled = true
date = "2017-09-10"
weight = 140
background = "primary"
form_name = "defaultContact"

title = "Join Library of Things Network"
#subtitle  = "*not working on demo page*"

# PostURL can be used with backends such as mailout from caddy
post_url = "https://formspree.io/xeqlqaww" #default: formspree.io
email = "marvinroman@protonmail.com"
button = "Send Request"
#netlify = false

# Optional google captcha
#[recaptcha]
#  sitekey = ""

[message]
  success = "Message successfully sent."
  error = "Message could not be sent. Please contact us at 442-236-2376 instead."
  field_error = "Please provide valid input."

# Only defined fields are shown in contact form
[[fields]]
  type = "text"
  name = "name"
  label = "Your Name *"
  required = true
  error = "Make sure to enter your name."

[[fields]]
  type = "text"
  name = "_replyto"
  validation_type = "email"
  label = "Your Email *"
  required = true
  error = "Make sure to enter a valid email."

[[fields]]
  type = "phone"
  name = "phone"
  validation_type = "regex"
  validation_regex = '^\(?\d{3}[\)\- ]{0,2}\d{3}[\- ]?\d{4}$'
  label = "Your Phone"
  required = false
  error = "Make sure to enter a valid phone number."

[[fields]]
  type = "textarea"
  name = "type"
  label = "What kind of items can you provide? *"
  required = true

# Optional hidden form fields
# Fields "page" and "site" will be autofilled
[[fields_hidden]]
  name = "page"

[[fields_hidden]]
  name = "type"
  value = "Library of Things"
+++
