+++
fragment = "content"
weight = 100

header = "Shopping Group"
title = "Grocery Shopping For Those At Higher Risk"
date = "2020-04-03"
categories = ["Food", "Groceries", "Risk Mitigation"]
+++

Join this group if your interested in reducing grocery trips for everybody and also those at a higher risk.

<!--more-->

## Recommendations for those shopping
1. Gather grocery bags from those your shopping for the day before. Ask them to put the grocery bags outside the door so that you don't need to interact with them.  
1. Wash hands before leaving the house.  
1. While shopping maintain 6 foot distance from other people.  
1. Wear a mask, hankerchief or other cloth over your mouth or nose.  
1. Be cognizant of your hands and what you touch and avoid touching your face, especially mouth, nose or eyes.
1. Always cough or sneeze into your sleave and never cough into your hands.  
1. Drop off groceries outside the door of those your shopping for or maintain 6 foot distance if interacting.  
1. Don't make extra work for grocery store clerks by splitting up onto different receipts. Instead use apps like [Splitwise](https://www.splitwise.com/), [Settle Up](https://settleup.io/) to calculate whats due.  
1. Try and only accept digital transactions for reimbursment if possible.  

# Recommendations for those receiving groceries
1. When bringing in groceries leave anything that doesn't need to be in the refridgerator in the bags for 24hrs.  
1. Wash any fresh produce with lots of water.  
1. After your finished unpacking wash your hands thoroughly with soap.  
1. Try not to touch any of the newly groceries again for a period of 24hrs.  