+++
fragment = "list"
weight = 250

background = "light"
count = 1000
section = "resources"
#summary = false # Default value is true
read_more = true # Default value is empty (empty: show when content is truncated, false to never show, true to always show)
tiled = true # Default value is false
display_date = false
display_categories = true

#subsections = true # Default to true. Shows subsection branch pages
#subsection_leaves = true # Default to false. Shows subsection leaf pages
+++
