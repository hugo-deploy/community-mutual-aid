+++
fragment = "content"
weight = 100

header = "Library of Things"
title = "Why buy when you can borrow?"
date = "2020-04-03"
categories = ["Mutual Aid", "Library", "Sharing"]
+++

Have tools or other household items that you rarely use, you can offer it to neigbors to borrow and make your own rules on use, length and replacement costs. 

<!--more-->

## Learn more about what a Library of Things is
[Learn More](https://www.shareable.net/history-as-a-commons-a-qa-with-chris-carlsson-of-shaping-san-francisco/)  