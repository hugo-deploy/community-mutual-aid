+++
title = "Tool Library"
weight = 40

[asset]
  icon = "fas fa-tools"
+++

Interested in having access to a wide range of tools, but not having to own every tool you might someday need? How might we operate a tool library that has well a wide array of well maintained tools, with proper safety training and gear?